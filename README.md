# Componentes de Primeng en Angular

## ¿Qué es?

PrimeNG es una colección de componentes de interfaz de usuario para Angular. Todos los widgets son de código abierto y de uso gratuito bajo la licencia MIT. PrimeNG es desarrollado por PrimeTek Informatics, un proveedor con años de experiencia en el desarrollo de soluciones UI de código abierto. Para noticias y actualizaciones de proyectos, síganos en Twitter y visite nuestro blog.

![](imagenes/primengventana.png)

En este proyecto vamos a poner un menú importado desde primeng en concreto este:
https://www.primefaces.org/primeng/#/megamenu
![](imagenes/menu.png)

Para instalar las librerías de de PrimeNG debemos escribir los siguientes comandos en la terminal dentro del proyecto.

```
npm install primeng --save
npm install primeicons --save
```

Tambien debemos referenciar los estilos de los componentes de primeng en el archivo angular.json
![](imagenes/styles.png)

de esta manera (es importante escribir en el *styles* de la parte de *build*):
![](imagenes/angularjson.png)

Una vez terminada las configuraciones previas ya podemos empezar a importar los componentes.

Como primer paso impotaremos las animaciones propias de angular.
![](imagenes/animaciones.png)
Luego importaremos el módulo correspondiente al menú que vamos a construir.

El módulo se encuentra detallado en la documentación del menú.
![](imagenes/documentacion.png)

Al final el archivo app.module.ts quedará similar a esto.
![](imagenes/appmodule.png)

Siguiendo con la documentación del menú en primeng debemos agregar los siguiente al archivo app.component.html
``` Html
<p-megaMenu [model]="items"></p-megaMenu>
```

En el archivo app.component.ts incluiremos una variable items, que también es la que se encuentra declarada en el componente *p-megaMenu*, de esta variable el menú tomara los elementos necesarios.

``` TypeScript
items: MenuItem[];
```
y lo inicilizamos en en el metodo ngOnInit del archivo app.component.ts

``` TypeScript
    ngOnInit() {
        this.items = [{
            label: 'File',
            items: [
                {label: 'New'},
                {label: 'Open'}
            ]
        },
        {
            label: 'Edit',
            items: [
                {label: 'Undo'},
                {label: 'Redo'}
            ]
        }];
    }
``` 
Al final el archivo quedará así:
![](imagenes/componentts.png)

Levantamos el proyecto con:
```
ng s
```

Y al final tenemos nuestro menú.
![](imagenes/menuangular.png)

Y eso fue todo

<a href="https://twitter.com/atpsito" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @atpsito </a><br>
<a href="https://www.linkedin.com/in/alexander-tigselema-ba1443124/" target="_blank"><img alt="Sígueme en LinkedIn" height="35" width="35" src="https://4.bp.blogspot.com/-0KtSvK3BydE/XCrIzgI3RqI/AAAAAAAAH_w/n_rr5DS92uk9EWEegcxeqAcSkV36OWEOgCLcBGAs/s1600/linkedin.png" title="Sígueme en LinkedIn"/> Alexander Tigselema</a><br>
<a href="https://www.instagram.com/atpsito" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> atpsito</a><br>